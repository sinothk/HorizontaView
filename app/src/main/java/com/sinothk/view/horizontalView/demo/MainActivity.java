package com.sinothk.view.horizontalView.demo;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.sinothk.view.horizontalView.HorizontalView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<String> data = new ArrayList<>();
        data.add("全部分类");
        data.add("生活小记");
        data.add("绿植养殖");
        data.add("宠物喂养");
        data.add("生活小记");
        data.add("绿植养殖");
        data.add("宠物喂养");

        HorizontalView<String> horizontalView = findViewById(R.id.appHorizontalView2);
        horizontalView.setData(data, new HorizontalView.HolderView<String>() {
            @Override
            public int getLayoutResId() {
                return R.layout.item_layout;
            }

            @Override
            public void bindingData(View view, String value, int index) {
                TextView tv = view.findViewById(R.id.tv);
                tv.setText(value);
            }

            @Override
            public void onItemClickListener(int position, String item) {
                Toast.makeText(MainActivity.this, "v = " + item, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
