package com.sinothk.view.horizontalView;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class HorizontalView<T> extends HorizontalScrollView {

    Context mContext;
    private int width;
    private int height;
    private int hv_margin_middle;
    private int hv_margin_start;
    private int hv_margin_end;

    private AttributeSet attrs;

    int defStyleAttr;
    int defStyleRes;

    public HorizontalView(Context context) {
        super(context);
        mContext = context;
    }

    public HorizontalView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.attrs = attrs;
        init(context, attrs);
    }

    public HorizontalView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.defStyleAttr = defStyleAttr;
    }

    public HorizontalView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.defStyleRes = defStyleRes;
    }

    private void init(Context context, AttributeSet attrs) {
        mContext = context;

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.HorizontalView, defStyleAttr, 0);

        width = typedArray.getDimensionPixelSize(R.styleable.HorizontalView_hv_width, 100);
        height = typedArray.getDimensionPixelSize(R.styleable.HorizontalView_hv_height, 100);

        hv_margin_middle = typedArray.getDimensionPixelSize(R.styleable.HorizontalView_hv_margin_middle, 10);
        hv_margin_start = typedArray.getDimensionPixelSize(R.styleable.HorizontalView_hv_margin_start, 10);
        hv_margin_end = typedArray.getDimensionPixelSize(R.styleable.HorizontalView_hv_margin_end, 10);
    }

    public void setData(final ArrayList<T> data, HolderView hView) {
        this.holderView = hView;

        LinearLayout linearLayout = new LinearLayout(mContext);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);

        for (int i = 0; i < data.size(); i++) {

            final T obj = data.get(i);

            final View itemView = LayoutInflater.from(mContext).inflate(holderView.getLayoutResId(), null);

            final int index = i;
            holderView.bindingData(itemView, obj, index);

            itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    holderView.onItemClickListener(index, obj);
                }
            });

            // ==================================== ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, height);
            if (i == 0) {
                lp.setMargins(hv_margin_start, hv_margin_middle, hv_margin_middle, hv_margin_middle);
            } else if (i == data.size() - 1) {
                lp.setMargins(0, hv_margin_middle, hv_margin_end, hv_margin_middle);
            } else {
                lp.setMargins(0, hv_margin_middle, hv_margin_middle, hv_margin_middle);
            }
            itemView.setLayoutParams(lp);
            linearLayout.addView(itemView, i);
        }

        addView(linearLayout);
    }

    HolderView<T> holderView;

    public interface HolderView<T> {

        int getLayoutResId();

        void bindingData(View view, T obj, int index);

        void onItemClickListener(int position, T obj);
    }
}
